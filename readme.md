# Projeto Motor de Indução

**O código fonte está dentro da pasta `inducao`. Abra essa pasta na IDE do Arduino.**

Firmware para Arduino Nano para monitoramento e acionamento de um Motor de Indução Trifásico.

Monitora tensão da rede, sequência e presença das fases e corrente do motor. Se algum dos parâmetros estiver alterado, desliga o motor.

Medição de tensão isolada usando optoacopladores.

Medição de correntes utilizando o sensor ACS720-30A.

Acionamento do motor utilizando um relé, que por sua vez aciona um contator trifásico.

## Bibliotecas

As bibliotecas necessárias já estão incluídas nesse repositório. Caso queira atualizar,
baixe a versão mais recente nos links abaixo.

* <https://github.com/gutierrezps/LiquidCrystal_PCF8574>
* <https://github.com/rkoptev/ACS712-arduino>
* <https://github.com/Testato/SoftwareWire>

## Integrantes - IFPB João Pessoa

* Gabriel Gutierrez
* Kaic Bezerra
* Luiz Otávio
* Arthur Melo
* Marcos Meira

## Convenções de código

<https://gist.github.com/gutierrezps/8f5f6ac67dc46443a127bec2af24c419>
