#include <Arduino.h>

/*******************************************************************************
 * BIBLIOTECAS
 *******************************************************************************/
#include "ACS712/ACS712.h"
#include "SoftwareWire/SoftwareWire.h"
#include "LiquidCrystal_PCF8574/LiquidCrystal_PCF8574.h"


/*******************************************************************************
 * REMOVER COMENTÁRIO DA LINHA ABAIXO PARA COMPILAR O CÓDIGO
 * PARA A SEGUNDA VERSÃO, COM MEDIÇÃO DE TENSÃO
 *******************************************************************************/
//#define SEGUNDA_VERSAO



/*******************************************************************************
 * DEFINIÇÕES DE PINOS DO ARDUINO
 *******************************************************************************/
const uint8_t pin_botaoOk = 2;
const uint8_t pin_botaoTela = 3;
const uint8_t pin_contactor = 4;
const uint8_t pin_i2cSda = 7;
const uint8_t pin_i2cScl = 6;



/*******************************************************************************
 * PARÂMETROS DO SISTEMA
 *******************************************************************************/

/**
 * Tempo máximo de espera, em milissegundos, para análise dos zeros.
 * Caso esse tempo seja excedido, significa que todas as fases estão ausentes.
 */
const uint16_t k_timeoutZerosMillis = 20;

/**
 * Diferença máxima, percentual, entre larguras de pulsos de zero
 * de cada fase. Caso a diferença seja maior que o valor estabelecido,
 * significa que há uma fase ausente.
 */
const float k_diferencaMinimaFaltaFase = 0.2;

/**
 * Intervalo, em milissegundos, entre leituras de todos os parâmetros
 * (zeros, tensões e correntes).
 */
const uint16_t k_delayLeiturasMillis = 100;

/**
 * Intervalo, em milissegundos, entre atualizações do display.
 */
const uint16_t k_delayAtualizacaoTelaMillis = 1000;

/**
 * Tempo de partida do motor, em segundos
 */
const uint8_t k_motorTempoPartidaSegs = 5;

/**
 * Corrente máxima do motor, em ampères. Caso a corrente medida
 * seja maior que este valor, significa que há sobrecorrente.
 */
const float k_correnteMaximaAmps = 7.0;

/**
 * Tensão mínima do motor, em volts. Caso a tensão medida seja
 * menor que este valor, significa que há subtensão.
 */
const uint16_t k_tensaoMinimaVolts = 210;

/**
 * Tensão máxima do motor, em volts. Caso a tensão medida seja
 * maior que este valor, significa que há sobretensão.
 */
const uint16_t k_tensaoMaximaVolts = 230;

/**
 * Endereço I2C do display LCD.
 */
const uint8_t k_lcdAddr = 0x27;

/**
 * Quantidade de amostras a serem colhidas dos parâmetros de cada fase,
 * para, em seguida, serem calculados os valores médios de cada parâmetro.
 */
#define MAX_QTD_AMOSTRAS_FASE 10



/*******************************************************************************
 * ESTRUTURAS DE DADOS UTILIZADAS
 *******************************************************************************/

/**
 * Possíveis estados de tensão de uma fase.
 */
enum class EstadoTensao : uint8_t {
    nenhum,         // valor inicial, quando ainda não foi feita nenhuma medição
    normal,         // tensão entre os valores mínimo e máximo estabelecidos
    subtensao,      // tensão menor do que o mínimo estabelecido
    sobretensao     // tensão maior do que o máximo estabelecido
};

/**
 * Struct para armazenamento de informações de uma fase
 */
struct Fase_t {
    uint8_t pinoTensao;         // pino de leitura da tensão
    uint8_t pinoCorrente;       // pino de leitura da corrente
    float corrente;             // valor de corrente médio
    uint16_t tensao;            // valor de tensão médio
    ACS712 *sensorCorrente;     // instância do sensor de corrente (biblioteca)
    bool isPresenteAmostras[MAX_QTD_AMOSTRAS_FASE];      // vetor de amostras de detecção da fase
    bool isPresente;            // se o valor for true, indica que a fase está presente
    bool isSobrecorrente;       // se o valor for true, indica que houve sobrecorrente na fase
    EstadoTensao estadoTensao;  // indica o estado da tensão (normal, sub ou sobretensão)
};

/**
 * Definição de valores possíveis para sequência de fases
 */
enum class Sequencia : uint8_t {
    nenhuma,        // quando não é possível estabelecer a sequência (fase ausente, por exemplo)
    horaria,        // sequência das fases ABC, BCA ou CAB
    antihoraria     // sequência das fases CBA, BAC ou ACB
};


/*******************************************************************************
 * VARIÁVEIS GLOBAIS
 *******************************************************************************/

/**
 * Vetor de amostras de sequência de fase.
 */
Sequencia g_sequenciaAmostras[MAX_QTD_AMOSTRAS_FASE] = {Sequencia::nenhuma};

/**
 * Valor da sequência das fases, obtido a partir do valor mais frequente
 * do vetor de amostras.
 */
Sequencia g_sequencia = Sequencia::nenhuma;

/**
 * Índice da amostra dos parâmetros de entrada que está sendo colhida.
 * Este valor é utilizado em todos os arrays de amostras de cada parâmetro.
 */
uint8_t g_nAmostraFase = 0;

/**
 * Se true, indica que o motor está ligado.
 */
bool g_isMotorLigado = false;

/**
 * Timer de partida do motor, em milissegundos.
 */
uint32_t g_timerPartidaMotorMillis = 0;

/**
 * Instante de tempo, em milissegundos, no qual foi feita a última leitura dos
 * parâmetros de entrada.
 */
uint32_t g_ultimaLeituraMillis = 0;

/**
 * Indica qual a tela do display que está sendo exibida.
 */
uint8_t g_telaInformacoes = 0;

/**
 * Instante de tempo, em milissegundos, no qual foi feita a última
 * atualização do display.
 */
uint32_t g_ultimaAtualizacaoTelaMillis = 0;

/**
 * Parâmetros de cada fase, de acordo com struct Fase_t.
 */
Fase_t g_fases[3] = {
    // Fase A
    {
        A3,     // pino de leitura de tensão da fase A
        A0,     // pino de leitura de corrente da fase A
        
        // demais valores iniciais, não precisa mexer nessa parte
        0.0, 0, NULL, {false}, false, false, EstadoTensao::nenhum
    },

    // Fase B
    {
        A4,     // pino de leitura de tensão da fase B
        A1,     // pino de leitura de corrente da fase B

        // demais valores iniciais, não precisa mexer nessa parte
        0.0, 0, NULL, {false}, false, false, EstadoTensao::nenhum
    },

    // Fase C
    {
        A5,     // pino de leitura de tensão da fase C
        A2,     // pino de leitura de corrente da fase C

        // demais valores iniciais, não precisa mexer nessa parte
        0.0, 0, NULL, {false}, false, false, EstadoTensao::nenhum
    }
};

/**
 * Mensagem de erro a ser exibida no display. Indica possível falha
 * que tenha acontecido.
 */
String g_mensagemErro = "";

/**
 * Instância do barramento I2C, usado para comunicação com display LCD.
 */
SoftwareWire g_softWire(pin_i2cSda, pin_i2cScl);

/**
 * Instância do display LCD
 */
LiquidCrystal_PCF8574 g_lcd(k_lcdAddr, &g_softWire);



/*******************************************************************************
 * PROTÓTIPOS DAS FUNÇÕES UTILIZADAS
 * 
 * VER COMENTÁRIOS ANTES DO CÓDIGO DE CADA FUNÇÃO.
 *******************************************************************************/

bool isFaseZero(uint8_t fase);

void analisaZeros(bool debug = false);

void medeTensoes();

void medeCorrentes();

void calculaMedias();

void atualizaDisplay();

void ligaMotor();

void desligaMotor();

bool motorEstaPartindo();

bool parametrosEstaoCorretos();



/*******************************************************************************
 * FUNÇÃO DE INICIALIZAÇÃO DO ARDUINO
 *******************************************************************************/

void setup()
{
    Serial.begin(115200);

    pinMode(pin_botaoOk, INPUT_PULLUP);
    pinMode(pin_botaoTela, INPUT_PULLUP);
    pinMode(pin_contactor, OUTPUT);
    digitalWrite(pin_contactor, HIGH);      // HIGH indica que o contactor está desligado

    g_softWire.begin();

    g_lcd.begin(16, 2);
    g_lcd.setBacklight(255);
    g_lcd.home();
    g_lcd.clear();

    g_lcd.print("Calibrando...");
    delay(1000);

    // Instancia e calibra sensores de corrente
    for (uint8_t fase = 0; fase < 3; ++fase) {
        g_fases[fase].sensorCorrente = new ACS712(ACS712_30A, g_fases[fase].pinoCorrente);
        g_fases[fase].sensorCorrente->calibrate();
    }

    g_lcd.clear();
}



/*******************************************************************************
 * FUNÇÃO PRINCIPAL DO ARDUINO, EXECUTADA INDEFINIDAMENTE
 *******************************************************************************/

void loop()
{
    // realiza uma amostra dos parâmetros de entrada, de acordo com o
    // intervalo determinado por k_delayLeiturasMillis
    if (millis() - g_ultimaLeituraMillis > k_delayLeiturasMillis) {
        analisaZeros();

#ifdef SEGUNDA_VERSAO
        medeTensoes();
#endif

        if (g_isMotorLigado) {
            medeCorrentes();

            if (!parametrosEstaoCorretos()) {
                desligaMotor();
            }
        }

        g_ultimaLeituraMillis = millis();

        g_nAmostraFase++;
        if (g_nAmostraFase == 10) {
            calculaMedias();
            g_nAmostraFase = 0;
        }
    }

    // verifica se o botão "tela" foi pressionado
    if (digitalRead(pin_botaoTela) == LOW) {
        // debounce
        delay(50);
        while(digitalRead(pin_botaoTela) == LOW) delay(1);
        delay(50);

        ++g_telaInformacoes;

        // pula a tela de medição de correntes se o motor estiver desligado
        if (g_telaInformacoes == 4 || (!g_isMotorLigado && g_telaInformacoes == 3)) {
            g_telaInformacoes = 0;
        }

        // força a realização de novas leituras
        g_ultimaLeituraMillis = 0;

        // força a atualização do display
        g_ultimaAtualizacaoTelaMillis = 0;
    }

    // verifica se o botão "ok" foi pressionado
    if (digitalRead(pin_botaoOk) == LOW) {
        // debounce
        delay(50);
        while(digitalRead(pin_botaoOk) == LOW) delay(1);
        delay(50);

        if (g_isMotorLigado) {
            desligaMotor();
        }
        else if (parametrosEstaoCorretos()) {
            ligaMotor();
        }
    }

    // verifica se há mensagem de erro
    if (g_mensagemErro.length() > 0) {
        g_lcd.clear();
        g_lcd.print("      FALTA      ");
        g_lcd.setCursor(0, 1);
        g_lcd.print(g_mensagemErro);
        

        // espera apertar botão da tela para limpar mensagem
        while(digitalRead(pin_botaoTela) == HIGH) delay(1);
        delay(50);
        while(digitalRead(pin_botaoTela) == LOW) delay(1);
        delay(50);

        g_mensagemErro = "";
        g_lcd.clear();
    }

    atualizaDisplay();
}



/*******************************************************************************
 * FUNÇÕES DE PROCESSAMENTO DOS SINAIS
 *******************************************************************************/

/**
 * Vê a tensão de uma fase está em zero
 * 
 * @param fase      a ser testada {0, 1, 2}
 * @return true     se a fase está passando por zero
 */
bool isFaseZero(uint8_t fase)
{
#ifdef SEGUNDA_VERSAO
    return analogRead(g_fases[fase].pinoTensao) > 900;
#else
    return digitalRead(g_fases[fase].pinoTensao) == LOW;
#endif
}



/**
 * Analisa os zeros dos sinais de tensão, para determinar
 * se todas as fases estão presentes, e para determinar a sequência
 * 
 * Após a execução, o parâmetro "isPresente" de cada fase é atualizado,
 * assim como a variável "g_sequencia".
 * 
 * @param debug     se true, imprime informações do processamento dos sinais
 */
void analisaZeros(bool debug)
{
    // medição de tempo
    uint32_t timestamp = 0;
    uint32_t t = 0;
    bool houveTimeout = true;

    /**
     * Armazena os instantes de tempo de descida e subida, respectivamente,
     * para cada fase.
     */
    uint32_t transicoes[3][2] = {{0, 0}, {0, 0}, {0, 0}};

    /**
     * Armazena o status atual da verificação dos tempos de subida e descida de
     * cada fase. 
     * O status 0 indica que nenhuma borda foi detectada.
     * O status 1 indica que somente a borda de descida foi registrada.
     * O status 2 indica que as bordas de subida e descida foram registradas.
     */
    uint8_t status[3] = {0, 0, 0};

    // conta quantas fases já tiveram suas bordas de descida e subida registradas
    uint8_t fasesRegistradas = 0;

    // indica qual foi a fase cujo zero foi detectado primeiro.
    // o valor -1 significa que nenhuma fase é a primeira ainda.
    int8_t primeiraFase = -1;

    // indica qual foi a fase cujo zero foi detectado em segundo lugar.
    // o valor -1 significa que nenhuma fase é a segunda ainda.
    int8_t segundaFase = -1;

    // indica qual a largura mínima detectada do pulso de zero
    uint16_t larguraMin = 0;

    // indica a qual fase pertence a largura mínima detectada
    uint8_t faseMin = 0;

    // indica qual a largura máxima detectada do pulso de zero
    uint16_t larguraMax = 0;

    // armazena temporariamente a presença de cada fase
    bool isPresente[3] = {false, false, false};

    // inicialização dos valores
    for (uint8_t fase = 0; fase < 3; ++fase) {
        g_fases[fase].isPresenteAmostras[g_nAmostraFase] = false;
        g_sequenciaAmostras[g_nAmostraFase] = Sequencia::nenhuma;
    }

    // Primeiro passo: esperar todas as fases estarem foram do zero ao mesmo tempo
    timestamp = millis();
    while (millis() - timestamp < k_timeoutZerosMillis) {
        if (!isFaseZero(0) && !isFaseZero(1) && !isFaseZero(2)) {
            houveTimeout = false;
            break;
        }
    }

    // Se as três fases não saíram do zero simultaneamente,
    // significa que nenhuma fase está presente
    if (houveTimeout) return;

    // Segundo passo: registrar transições para o zero de cada fase
    houveTimeout = true;
    timestamp = micros();

    do {
        t = micros() - timestamp;
        for (uint8_t fase = 0; fase < 3; ++fase) {
            if (status[fase] == 0 && isFaseZero(fase)) {
                // se for a borda de descida
                transicoes[fase][0] = t;
                status[fase] = 1;

                // registra a primeira e segunda fases a passar no zero
                if (primeiraFase < 0) {
                    primeiraFase = fase;
                }
                else if (segundaFase < 0) {
                    segundaFase = fase;
                }
            }
            else if (status[fase] == 1 && !isFaseZero(fase)) {
                // se for a borda de subida
                transicoes[fase][1] = t;
                status[fase] = 2;

                fasesRegistradas++;
                if (fasesRegistradas == 3) {
                    houveTimeout = false;
                    break;
                }
            }
        }
    } while (micros() - timestamp < (k_timeoutZerosMillis * 1000));

    if (houveTimeout) return;

    // se debug estiver ativado, imprime na Serial os tempos das bordas
    for (uint8_t fase = 0; debug && fase < 3; ++fase) {
        Serial.print("Fase");
        Serial.print((char)('A' + fase));
        Serial.print(": ");
        Serial.print(transicoes[fase][0]);
        Serial.print(" - ");
        Serial.print(transicoes[fase][1]);
        Serial.print(" = ");
        Serial.println(transicoes[fase][1] - transicoes[fase][0]);
    }

    // Terceiro passo: calcular largura dos pulsos de zero para cada fase
    for (uint8_t fase = 0; fase < 3; ++fase) {
        uint16_t delta = transicoes[fase][1] - transicoes[fase][0];

        if (fase == 0) larguraMin = delta;

        if (delta < larguraMin) {
            larguraMin = delta;
            faseMin = fase;
        }

        if (delta > larguraMax) {
            larguraMax = delta;
        }
    }

    float diferenca = (float)(larguraMax - larguraMin)/larguraMin;

    if (diferenca > k_diferencaMinimaFaltaFase) {
        if (debug) {
            Serial.print("############################ FALTA ");
            Serial.println(diferenca);
            Serial.println("======");
        }
        
        switch (faseMin) {
            case 0 :
                isPresente[0] = true;
                isPresente[1] = true;
                break;

            case 1 :
                isPresente[1] = true;
                isPresente[2] = true;
                break;

            case 2 :
                isPresente[0] = true;
                isPresente[2] = true;
                break;
        }
    }
    else {
        isPresente[0] = true;
        isPresente[1] = true;
        isPresente[2] = true;

        if ( (primeiraFase == 0 && segundaFase == 2)
            || (primeiraFase == 2 && segundaFase == 1)
            || (primeiraFase == 1 && segundaFase == 0)
        ) {
            g_sequenciaAmostras[g_nAmostraFase] = Sequencia::horaria;
        }
        else {
            g_sequenciaAmostras[g_nAmostraFase] = Sequencia::antihoraria;
        }
    }

    for (uint8_t fase = 0; fase < 3; ++fase) {
        g_fases[fase].isPresenteAmostras[g_nAmostraFase] = isPresente[fase];
    }
}



#ifdef SEGUNDA_VERSAO
/**
 * Mede as tensões AC de cada fase
 * 
 * Após executada, os parâmetros "tensao" de cada fase são atualizados.
 * 
 * Faz-se a média de MAX_QTD_AMOSTRAS_FASE do valor mínimo de tensão
 * do pino analógico. Em seguida calcula-se o valor pico-pico
 * (5V - valor lido), para então obter-se a tensão AC de entrada equivalente,
 * de acordo com equação de regressão obtida por meio de experimento.
 * 
 * O valor mínimo de cada amostra é obtido por meio de 55 leituras analógicas
 * de cada fase. Visto que, de acordo com a documentação do Arduino, a função
 * analogRead() leva 100us para realizar a leitura, o tempo para coleta
 * das 64*3 amostras é aproximadamente igual a um período da frequência
 * da rede elétrica, 60 Hz. (55 * 3 * 100us = 16,5 ms // 1/60Hz = 16,67 ms)
 * 
 * @see https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/
 */
void medeTensoes()
{
    uint32_t somaAmostras[3] = {0, 0, 0};

    for (uint8_t amostra = 0; amostra < MAX_QTD_AMOSTRAS_FASE; ++amostra) {
        uint16_t minAmostra[3] = {1023, 1023, 1023};
        
        for (uint8_t i = 0; i < 55; ++i) {
            for (uint8_t fase = 0; fase < 3; ++fase) {
                uint16_t amostra = analogRead(g_fases[fase].pinoTensao);
                if (amostra < minAmostra[fase]) {
                    minAmostra[fase] = amostra;
                }
            }
        }

        somaAmostras[0] += minAmostra[0];
        somaAmostras[1] += minAmostra[1];
        somaAmostras[2] += minAmostra[2];
    }

    for (uint8_t fase = 0; fase < 3; ++fase) {
        // média das amostras
        somaAmostras[fase] /= MAX_QTD_AMOSTRAS_FASE;
        
        // valor pico-pico
        uint16_t adc = 1023 - somaAmostras[fase];
    
        // equação de regressão
        g_fases[fase].tensao = 64.307 * 5.0 * ((float)adc/1023.0) + 28.257;
    }
}
#endif



/**
 * Realiza medições das correntes de cada fase
 * 
 * Após a execução, os parâmetros "corrente" e "isSobrecorrente" de
 * cada fase são atualizados.
 */
void medeCorrentes()
{
    uint8_t qtdAmostras = 0;

    g_fases[0].corrente = 0;
    g_fases[1].corrente = 0;
    g_fases[2].corrente = 0;

    do {
        for (uint8_t fase = 0; fase < 3; ++fase) {
            g_fases[fase].corrente += g_fases[fase].sensorCorrente->getCurrentAC(60);
        }
        
        ++qtdAmostras;
    } while (qtdAmostras < MAX_QTD_AMOSTRAS_FASE);

    for (uint8_t fase = 0; fase < 3; ++fase) {
        g_fases[fase].corrente /= (float) MAX_QTD_AMOSTRAS_FASE;

        if (g_fases[fase].corrente > k_correnteMaximaAmps) {
            g_fases[fase].isSobrecorrente = true;
        }
        else {
            g_fases[fase].isSobrecorrente = false;
        }
    }
}



/**
 * Verifica a presença e a sequência das fases, a partir dos vetores
 * de amostras.
 */
void calculaMedias()
{
    uint8_t somaPresente[3] = {0, 0, 0};

    for (uint8_t fase = 0; fase < 3; ++fase) {
        for (uint8_t amostra = 0; amostra < MAX_QTD_AMOSTRAS_FASE; ++amostra) {
            somaPresente[fase] += g_fases[fase].isPresenteAmostras[amostra];
        }
        g_fases[fase].isPresente = somaPresente[fase] > (0.7 * MAX_QTD_AMOSTRAS_FASE);
    }

    uint8_t contaSequencia[3] = {0};
    uint8_t seqMax = 0;

    for (uint8_t amostra = 0; amostra < MAX_QTD_AMOSTRAS_FASE; ++amostra) {
        contaSequencia[(uint8_t) g_sequenciaAmostras[amostra]]++;
    }

    for (uint8_t seq = 1; seq < 3; ++seq) {
        if (contaSequencia[seq] > contaSequencia[seqMax]) seqMax = seq;
    }
    
    if (seqMax == (uint8_t) Sequencia::antihoraria) g_sequencia = Sequencia::antihoraria;
    else if (seqMax == (uint8_t) Sequencia::horaria) g_sequencia = Sequencia::horaria;
    else g_sequencia = Sequencia::nenhuma;
}



/**
 * Atualiza o display LCD, de acordo com a tela sendo mostrada
 * 
 * A tela é definida pela variável global "g_telaInformacoes", que pode
 * assumir os seguintes valores:
 * 
 * 0 - presença das fases (primeira versão) / tensão das fases (segunda versão)
 * 1 - sequência de fases
 * 2 - estado do motor (ligado/desligado)
 * 3 - correntes do motor
 */
void atualizaDisplay()
{
    // atualiza a tela apenas se já se passou o tempo determinado pelo
    // parâmetro k_delayAtualizacaoTelaMillis
    if (millis() - g_ultimaAtualizacaoTelaMillis < k_delayAtualizacaoTelaMillis) {
        return;
    }

    g_ultimaAtualizacaoTelaMillis = millis();

    g_lcd.setCursor(0, 0);

#ifdef SEGUNDA_VERSAO
    if (g_telaInformacoes == 0) {
        g_lcd.print(F("tensao          "));

        g_lcd.setCursor(0,1); g_lcd.print(F("               "));

        uint8_t col[3] = {0, 6, 12};

        for (uint8_t fase = 0; fase < 3; ++fase) {
            g_lcd.setCursor(col[fase],1);

            if (g_fases[fase].tensao < 50) g_lcd.print("< 50");
            else g_lcd.print(g_fases[fase].tensao);
        }
    }
#else
    if (g_telaInformacoes == 0) {
        g_lcd.print("Detecta fase   ");
        g_lcd.setCursor(0,1);
        g_lcd.print("R:");
        g_lcd.print((int) g_fases[0].isPresente);
        g_lcd.print(" S:");
        g_lcd.print((int) g_fases[1].isPresente);
        g_lcd.print(" T:");
        g_lcd.print((int) g_fases[2].isPresente);
        g_lcd.print("    ");
    }
#endif

    else if (g_telaInformacoes == 1) {
        if (g_sequencia == Sequencia::nenhuma) {
            g_lcd.print(F(" sem sequencia  "));
            g_lcd.setCursor(0,1);
            g_lcd.print(F("  falta fase "));

            if (!g_fases[0].isPresente) g_lcd.print("R ");
            else if (!g_fases[1].isPresente) g_lcd.print("S ");
            else g_lcd.print("T ");
        }
        else {
            g_lcd.print(F("   sequencia    "));
            g_lcd.setCursor(0,1);

            if (g_sequencia == Sequencia::horaria) {
                g_lcd.print(F("    horaria     "));
            }
            else {
                g_lcd.print(F("  antihoraria   "));
            }
        }
    }
    else if (g_telaInformacoes == 2) {
        g_lcd.print(F("motor          "));
        g_lcd.setCursor(0,1);
        g_lcd.print(g_isMotorLigado ? F("ligado   ") : F("desligado"));
        g_lcd.print("      ");
    }
    else if (g_telaInformacoes == 3) {
        g_lcd.print(F("corrente /max "));
        g_lcd.print(k_correnteMaximaAmps, 0);

        g_lcd.setCursor(0,1); g_lcd.print(F("               "));
        g_lcd.setCursor(0,1);
        g_lcd.print(g_fases[0].corrente, 1);

        g_lcd.setCursor(6,1);
        g_lcd.print(g_fases[1].corrente, 1);

        g_lcd.setCursor(12,1);
        g_lcd.print(g_fases[2].corrente, 1);
    }
}



/**
 * Realiza o acionamento do motor e inicia o timer da partida
 */
void ligaMotor()
{
    g_timerPartidaMotorMillis = millis();
    digitalWrite(pin_contactor, LOW);
    g_isMotorLigado = true;
}



/**
 * Desliga o motor
 */
void desligaMotor()
{
    digitalWrite(pin_contactor, HIGH);
    g_isMotorLigado = false;
}



/**
 * Verifica se o motor está em processo de partida, determinado
 * pelo parâmetro k_motorTempoPartidaSegs
 * 
 * @return true     se o motor ainda está partindo
 */
bool motorEstaPartindo()
{
    if (!g_isMotorLigado) return false;

    if (g_timerPartidaMotorMillis == 0) return false;

    // se o motor está ligado a mais tempo do que o tempo de partida,
    // reseta o contador para parar de executar esse cálculo
    if (millis() - g_timerPartidaMotorMillis > (k_motorTempoPartidaSegs * 1000)) {
        g_timerPartidaMotorMillis = 0;
        return false;
    }

    return true;
}



/**
 * Verifica se algum dos parâmetros está fora da normalidade. Se sim,
 * atualiza a variável "g_mensagemErro".
 * 
 * Considera-se normalidade quando:
 *  - todas as fases estão presentes
 *  - a sequência das fases é horária
 *  - as tensões de cada fase estão entre os limites inferior e superior configurados
 *  - as correntes de cada fase estão abaixo do limite configurado
 * 
 * Quando algum parâmetro estiver anormal, a variável "g_mensagemErro"
 * é atualizada com uma string que descreve o primeiro problema encontrado. Assim,
 * deve-se verificar se há erro executando-se "g_mensagemErro.length() > 0".
 * 
 * @return true     se todos os parâmetros estão OK
 */

bool parametrosEstaoCorretos()
{
    // não verifica os parâmetros se o motor estiver em partida
    if (motorEstaPartindo()) return true;

    char letraFase[3] = {'R', 'S', 'T'};

    // verifica se há alguma fase ausente
    for (uint8_t fase = 0; fase < 3; ++fase) {
        if (!g_fases[fase].isPresente) {
            g_mensagemErro = F("fase ");
            g_mensagemErro += letraFase[fase];
            g_mensagemErro += F(" ausente");
            return false;
        }
    }

    // verifica se a sequência das fases está correta
    if (g_sequencia != Sequencia::horaria) {
        if (g_sequencia == Sequencia::antihoraria) {
            g_mensagemErro = F("seq. antihoraria");
        }
        else {
            g_mensagemErro = F("seq. indefinida");
        }
        return false;
    }

#ifdef SEGUNDA_VERSAO
    // verifica as tensões de cada fase
    for (uint8_t fase = 0; fase < 3; ++fase) {
        if (g_fases[fase].estadoTensao == EstadoTensao::normal) {
            continue;
        }

        if (g_fases[fase].estadoTensao == EstadoTensao::subtensao) {
            g_mensagemErro = F("subtensao ");
            g_mensagemErro += letraFase[fase];
            return false;
        }

        if (g_fases[fase].estadoTensao == EstadoTensao::sobretensao) {
            g_mensagemErro = F("sobretensao ");
            g_mensagemErro += letraFase[fase];
            return false;
        }

        g_mensagemErro = F("tensao indef. ");
        g_mensagemErro += letraFase[fase];
        return false;
    }
#endif

    if (!g_isMotorLigado) return true;

    // verifica as correntes de cada fase
    for (uint8_t fase = 0; fase < 3; ++fase) {
        if (g_fases[fase].isSobrecorrente) {
            g_mensagemErro = F("sobrecorrente ");
            g_mensagemErro += letraFase[fase];
            return false;
        }
    }

    return true;
}
